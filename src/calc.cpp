#include <iostream>
#include <sstream>
#include <vector>

#include <boost/phoenix.hpp>
#include <boost/spirit/home/qi.hpp>
#include <boost/variant.hpp>

namespace qi = boost::spirit::qi;

BOOST_PHOENIX_ADAPT_FUNCTION(double, pow_phoenix, std::pow, 2);

class expression_parser : public qi::grammar<std::string::iterator, double(),
                              qi::ascii::space_type> {
public:
    expression_parser()
        : expression_parser::base_type(expression, "expression") {
        using boost::phoenix::construct;
        using boost::phoenix::val;
        using qi::char_;
        using qi::labels::_1;
        using qi::labels::_2;
        using qi::labels::_3;
        using qi::labels::_4;
        using qi::labels::_a;
        using qi::labels::_val;

        value = qi::double_[_val = _1];
        base = value[_val = _1] | (char_('(') > sum[_val = _1] > char_(')'));
        factor = base[_val = _1] >>
            -(char_('^') > base[_val = pow_phoenix(_val, _1)]);
        term = factor[_val = _1] >> *((char_('*') > factor[_val *= _1]) |
                                        (char_('/') > factor[_val /= _1]));
        sum = term[_val = _1] >> *((char_('+') > term[_val += _1]) |
                                     (char_('-') > term[_val -= _1]));
        expression = sum[_val = _1];

        expression.name("expression");
        sum.name("sum");
        term.name("term");
        factor.name("factor");
        base.name("base");
        value.name("value");

        qi::on_error<qi::fail>(expression,
            (boost::phoenix::ref(on_error_called) = true,
                std::cout << val("Parse error: expected ") << _4
                          << val(" at column ") << (_3 - _1 + 1) << std::endl));
    }

    using rule_type =
        qi::rule<std::string::iterator, double(), qi::ascii::space_type>;

    rule_type expression;
    rule_type sum;
    rule_type term;
    rule_type factor;
    rule_type base;
    rule_type value;

    bool on_error_called{false};
};

int main() {
    try {
        expression_parser parser;

        while (true) {
            std::string line;
            std::cout << "> " << std::flush;
            std::getline(std::cin, line);
            if (line == "exit") {
                return 0;
            }

            double answer = 0.0;
            parser.on_error_called = false;
            auto iter = line.begin();
            bool is_ok = qi::phrase_parse(
                iter, line.end(), parser, qi::ascii::space, answer);
            if (iter != line.end()) {
                is_ok = false;
            }
            if (is_ok) {
                std::cout << answer << std::endl;
            } else {
                if (!parser.on_error_called) {
                    std::cout << "Parse error: unexpected token at column "
                              << (iter - line.begin() + 1) << std::endl;
                }
            }
        }
    } catch (std::exception& e) {
        std::cout << "exception: " << e.what() << std::endl;
        return 1;
    }
}
